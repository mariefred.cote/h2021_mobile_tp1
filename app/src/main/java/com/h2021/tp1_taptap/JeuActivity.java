package com.h2021.tp1_taptap;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.time.Duration;
import java.time.Instant;
import java.util.Timer;

public class JeuActivity extends AppCompatActivity implements View.OnClickListener {
    static final String PSEUDO = "pseudo";
    static final String SCORE = "score";
    static final String MS = " ms";
    SharedPreferences prefs;

    private Button btStart, btSetting,bt1, bt2, bt3, bt4;
    private TextView tvScore, tvJoueur, tvPseudo;
    private Boolean bt1Clique = false;
    private Boolean bt2Clique = false;
    private Boolean bt3Clique = false;
    private Boolean bt4Clique = false;
    private long timer = 0;
    private long score;
    private Instant start;
    //private String pseudo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        bindViews();

        btStart.setOnClickListener(this);
        btSetting.setOnClickListener(this);
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        bt4.setOnClickListener(this);

        prefs = getSharedPreferences("Sauvegarde", MODE_PRIVATE);

        Intent intent = getIntent();
        tvPseudo.setText(intent.getStringExtra(PSEUDO));


        if (savedInstanceState == null) {
            score = 0;
        }

        tvScore.setText(Long.toString(score));


    }

    private void bindViews() {
        btSetting =  findViewById(R.id.bt_setting);
        btStart =  findViewById(R.id.bt_start);
        tvScore =  findViewById(R.id.tv_score);
        bt1 =  findViewById(R.id.bt_1);
        bt2 =  findViewById(R.id.bt_2);
        bt3 =  findViewById(R.id.bt_3);
        bt4 =  findViewById(R.id.bt_4);
        tvJoueur =  findViewById(R.id.tv_joueur);
        tvPseudo =  findViewById(R.id.tv_pseudo);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.bt_1:
                if (timer == 0){
                    start = Instant.now();
                    bt1Clique = true;
                    bt1.setText(R.string.start);
                    bt1.setBackgroundColor(Color.parseColor("#81c784"));
                }
                break;
            case R.id.bt_2:
                if (bt1Clique && !bt2Clique){
                    Instant bt2Time = Instant.now();
                    timer = Duration.between(start, bt2Time).toMillis();
                    bt2.setText(Long.toString(timer) + MS);
                    bt2.setBackgroundColor(Color.parseColor("#fff280"));
                    bt2Clique = true;
                }
                break;
            case R.id.bt_3:
               if (bt1Clique && bt2Clique && !bt3Clique){
                   Instant bt3Time = Instant.now();
                   timer = Duration.between(start, bt3Time).toMillis();
                   bt3.setText(Long.toString(timer) + MS );
                   bt3.setBackgroundColor(Color.parseColor("#ffcc80"));
                   bt3Clique = true;
               }
                break;
            case R.id.bt_4:
                if (bt1Clique && bt2Clique && bt3Clique && !bt4Clique){
                    Instant bt4Time = Instant.now();
                    timer = Duration.between(start, bt4Time).toMillis();
                    bt4.setText(Long.toString(timer) + MS);
                    bt4.setBackgroundColor(Color.parseColor("#f98e86"));
                    bt4Clique = true;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(PSEUDO, tvPseudo.getText().toString());
                    if (score == 0 || timer < score ){
                        score = timer;
                        tvScore.setText(Long.toString(score));
                        editor.putString(SCORE,tvScore.getText().toString());
                    }
                    editor.apply();
                    if (timer >= 1000 && timer <= 2000){
                        intent = new Intent(JeuActivity.this, ImageActivity.class);
                        intent.putExtra(SCORE,Long.toString(timer));
                        intent.putExtra(PSEUDO,tvPseudo.getText().toString());
                        startActivity(intent);
                    }
                }
                break;
            case R.id.bt_setting:
                intent = new Intent(JeuActivity.this, SettingsActivity.class);
                intent.putExtra(PSEUDO, tvPseudo.getText().toString());
                intent.putExtra(SCORE, Long.toString(score));
                startActivity(intent);
                break;
            case  R.id.bt_start:
                if (bt4Clique){
                    bt1Clique = false;
                    bt2Clique = false;
                    bt3Clique = false;
                    bt4Clique = false;
                    bt1.setText("1");
                    bt2.setText("2");
                    bt3.setText("3");
                    bt4.setText("4");;
                    bt1.setBackgroundColor(Color.parseColor("#4caf50"));
                    bt2.setBackgroundColor(Color.parseColor("#ffeb3b"));
                    bt3.setBackgroundColor(Color.parseColor("#ff9800"));
                    bt4.setBackgroundColor(Color.parseColor("#f44336"));
                    timer = 0;
                }
                break;
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(PSEUDO, tvPseudo.getText().toString());
        outState.putString(SCORE,tvScore.getText().toString());
        super.onSaveInstanceState(outState);

    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        tvPseudo.setText(savedInstanceState.getString(PSEUDO));
        tvScore.setText(savedInstanceState.getString(SCORE));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        score = Long.parseLong(prefs.getString(SCORE, "0"));
        tvPseudo.setText(prefs.getString(PSEUDO, "rien"));
        tvScore.setText(Long.toString(score));

    }
}
