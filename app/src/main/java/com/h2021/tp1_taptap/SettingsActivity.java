package com.h2021.tp1_taptap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.time.Instant;

import static java.net.Proxy.Type.HTTP;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    static final String PSEUDO = "pseudo";
    static final String SCORE = "score";
    static final String MESSAGE = "Voici mon meilleur score: ";
    String score;
    private Button btReset, btOkPseudo;;
    private EditText etChangePseudo;
    private ImageButton btShare;
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        prefs = getSharedPreferences("Sauvegarde", MODE_PRIVATE);
        btReset =  findViewById(R.id.bt_reset);
        etChangePseudo =  findViewById(R.id.et_ChangePseudo);
        btOkPseudo =  findViewById(R.id.bt_okPseudo);
        btShare =  findViewById(R.id.bt_share);

        btOkPseudo.setOnClickListener(this);
        btReset.setOnClickListener(this);
        btShare.setOnClickListener(this);

        Intent intent = getIntent();
        etChangePseudo.setText(intent.getStringExtra(PSEUDO));
        score = intent.getStringExtra(SCORE);


    }

    @Override
    public void onClick(View v) {
        SharedPreferences.Editor editor = prefs.edit();
        switch (v.getId()){
            case R.id.bt_okPseudo:
                editor.putString(PSEUDO, etChangePseudo.getText().toString());
                editor.apply();
                break;
            case R.id.bt_reset:
                editor.putString(SCORE,"0");
                editor.apply();
                break;
            case R.id.bt_share:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra("sms_body", MESSAGE + score + "ms" );
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                break;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(PSEUDO, etChangePseudo.getText().toString());
        super.onSaveInstanceState(outState);

    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        etChangePseudo.setText(savedInstanceState.getString(PSEUDO));
        super.onRestoreInstanceState(savedInstanceState);
    }
}
