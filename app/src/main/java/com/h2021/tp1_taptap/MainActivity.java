package com.h2021.tp1_taptap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etPseudo, etPassword;
    private Button btLogin;
    static final String BOT = "bot";
    static final String MOT_DE_PASSE = "123";
    static final String PSEUDO = "pseudo";
    static final String ERREUR_CONNEXION = "Mauvais Pseudo/Mot de passe";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etPassword = findViewById(R.id.et_password);
        etPseudo = findViewById(R.id.et_pseudo);
        btLogin = findViewById(R.id.bt_login);

        btLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String pseudo = etPseudo.getText().toString();
        String password = etPassword.getText().toString();
        if (pseudo.equals(BOT) && password.equals(MOT_DE_PASSE)){
            Intent intent = new Intent(MainActivity.this,JeuActivity.class);
            intent.putExtra(PSEUDO, BOT);
            startActivity(intent);
        } else {
            Toast.makeText(MainActivity.this,ERREUR_CONNEXION,Toast.LENGTH_LONG).show();
        }
    }
}