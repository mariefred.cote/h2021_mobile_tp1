package com.h2021.tp1_taptap;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;


public class ImageActivity extends AppCompatActivity {

    private TextView tvScore;
    static final String SCORE = "score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);


        tvScore = findViewById(R.id.tv_scoreImage);

        Intent intent = getIntent();
        String score = intent.getStringExtra(SCORE);

        tvScore.setText(score + " ms");
    }

}
